#!/bin/bash -v
# Basic test of pyCoCoDM.
#
# Four short trajectory files of the alanine pentapeptide are analysed by CoCo
# and eight new structures are generated, in .pdb format

pyCoCoDM --nompi -i md?.dcd -t Ala5.pdb -s 'all' -g 30 -d 3 -n 8 -o Ala5cocoDM.pdb -l test.log -v --nsamples 1000



