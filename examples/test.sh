#!/bin/bash -v
# Basic test of pyCoCo.
#
# Four short trajectory files of the alanine pentapeptide are analysed by CoCo
# and eight new structures are generated, in .pdb format

pyCoCo --nompi -i md?.dcd -t Ala5.pdb -s 'all' -g 30 -d 3 -n 8 -o Ala5coco.pdb -l test.log --regularize -v



