#
# The Python script runs a simple version of a CoCo-MD workflow (see Shkurti
# et al. JCTC 2019).
#
# The molecule is penta-alanine, and for speed the simulations are run in
# vacuo, and with shorter MD runs than would be used for real.
#
# The workflow is written using the  Crossflow (aka 'xbowflow') library, and
# also uses MDTraj.
#
from xbowflow import xflowlib
import mdtraj as mdt
from extasycoco import complement

# set number of cycles, and number of replicates
maxcycles = 5
nreps = 5

# Load the input files
startcrds = xflowlib.load('penta.rst7')
topfile = xflowlib.load('penta.prmtop')
minin = xflowlib.load('min.in')
mdin = xflowlib.load('md.in')

# Create a kernel for the Amber energy minimisation job.
#
# This kernel definition produces a Python function that takes a set of
# starting coordinates as the only argument, and returns a set of minimized
# coordinates.
minimize = xflowlib.SubprocessKernel('sander -i min.in -c start.rst7 -p topology.prmtop -ref refc.rst7 -r minimized.rst7')
minimize.set_inputs(['min.in', 'start.rst7', 'topology.prmtop', 'refc.rst7'])
minimize.set_outputs(['minimized.rst7'])
minimize.set_constant('min.in', minin)
minimize.set_constant('topology.prmtop', topfile)
minimize.set_constant('start.rst7', startcrds)

# Create a kernel for the Amber MD job.
#
# This kernel definition produces a Python function that takes a set of 
# staring coordinates as the only argument, and returns a trajectory.
md = xflowlib.SubprocessKernel('sander -i md.in -c start.rst7 -p topology.prmtop -x trajectory.nc')
md.set_inputs(['md.in', 'start.rst7', 'topology.prmtop'])
md.set_outputs(['trajectory.nc'])
md.set_constant('md.in', mdin)
md.set_constant('topology.prmtop', topfile)

# Create an MDTraj trajectory that will grow each cycle. Start with just the
# start coordinates:
traj = mdt.load('penta.rst7', top='penta.prmtop')
#
# Main loop begins here:
#
logfile = open('CoCo-MD.log', 'w')
for cycle in range(maxcycles):
    # This next line is the CoCo step:
    new_structures = complement(traj, npoints=nreps, refine=True, logfile=logfile)
    print('Starting CoCo-MD cycle {}'.format(cycle))
    for i, structure in enumerate(new_structures):
        print('  Running MD on replicate {}'.format(i))
        minimized = minimize.run(structure)
        trajfile = md.run(minimized)
        new_trajectory = mdt.load(trajfile.as_file(), top='penta.prmtop')
        traj += new_trajectory
    print('Cycle {} complete'.format(cycle))
logfile.close()
traj.save('ensemble.nc')
print('Ensemble written to file ensemble.nc')
