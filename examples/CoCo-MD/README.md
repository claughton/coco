A simple example of a CoCo-MD workflow (see Shkurti et al. JCTC 2019)

The workflow is as follows:

1. Starting with a single structure (of the alanine pentapeptide), eight short 
replicate MD simulations are run, using AMBER.
2. The eight trajectory files are analysed using CoCo and eight new structures,
corresponding to unsampled regions in the conformational distribution, are 
generated.
3. Further MD is run on each of these
4. Back to step 2, including both newly-generated and previous cycles of 
trajectory files in the analysis.

To run the workflow, type:

python CoCo-MD.py

When complete, two new files are generated:

CoCO-MD.log: A report on the CoCo analysisn of the ensemble each cycle. You
should see in particular that each cycle the total variance in the ensemble is increased.

ensemble.nc: The final ensemble of conformations, in AMBER netcdf format.
