#!/usr/bin/env python
#
# Example of use use of the core coco method 'complement' in association
# with MDTraj
#
# The NMR structures in 1cfc are analysed and ten new models generated.
#
import mdtraj as mdt
from extasycoco import complement
t = mdt.load('1cfc.pdb')
r = complement(t, npoints=10, refine=True)
r.save('1cfc_coco.pdb')
