#!/bin/bash -v
# Basic test of pyCoCo.
#
# Four short trajectory files of the alanine pentapeptide are analysed by CoCo
# and eight new structures are generated, in .pdb format
# The analysis ignores the distribution in the 1st PC

pyCoCo --nompi -i md?.dcd -t Ala5.pdb -s 'all' -g 30 -d 3 -n 8 --skip 1 -o Ala5coco_s1_.pdb -l test.log --regularize -v



