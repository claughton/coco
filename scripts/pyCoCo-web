#!/usr/bin/env python 
from __future__ import print_function

from extasycoco._version import __version__

import logging as log
import sys
import os
import os.path as op
import numpy as np
import argparse
import glob
import time
import mdtraj as mdt
import numpy as np
import copy
from MDPlus.analysis import pca
from MDPlus.analysis import mapping 
from extasycoco import new_points

def coco_web(args):
    '''
    The command line implementation of the CoCo procedure for the web service. 
    Should be invoked as:

    pyCoCo-web -i pdbin -o pdbout -l logfile [--regularize]
    where:
        pdbin    is the input multimodel pdb file
        pdbout   is the output new multimodel pdb file
        logfile  contains detailed analysis data.

        regularize specifies that generated structures should (if possible)
                 have their bond lengths and angles regularised.
    '''

    try:
        logfile = open(args.logfile,'w')
    except IOError as e:
        print(e)
        exit(-1)
    logfile.write("*** pyCoCo 2.0 ***\n\n")
    logfile.write("Please cite: Laughton et al., Proteins, 2009, 75, 206-216.\n\n")

    start_time = time.time()
    
    try:
        t = mdt.load(args.pdbin)
    except (IOError, TypeError)  as e:
        print(e)
        exit(-1)
    except:
        raise
    
    n_structures = len(t)
    if n_structures < 3:
        logfile.write("Error: the input ensemble only contains {} structures;\n".format(len(t)))
        logfile.write("the CoCo procedure requires at least 5.")
        logfile.write('\n')
        exit(1)
    elif n_structures > 50:
        logfile.write("Error: the input ensemble contains {} structures;\n".format(len(t)))
        logfile.write("the CoCo procedure requires a maximum of 50.")
        logfile.write('\n')
        exit(1)
    else:
        logfile.write("Input ensemble  contains {} structures\n".format(len(t)))

    p = pca.fromtrajectory(t, quality=95)
    n_vecs = p.n_vecs
    if n_vecs < 3:
        logfile.write("Error: the input ensemble has a complexity of only {}\n".format(n_vecs))
        logfile.write("CoCo needs a complexity of at least 3.\n")
        exit(1)
    else:
        logfile.write("Complexity of input ensemble: {}\n".format(n_vecs))

            
    m = mapping.Map(p.projs[:2].T, resolution=5)
    coverage = int(100 * float(len(np.where(m._H > 0)[0])) / 25)
    logfile.write("Coverage of the input ensemble: {}%\n\n".format(coverage))

    m = mapping.Map(p.projs[:3].T, resolution=5)
    cp = new_points(m, npoints=n_structures)

    newxyz = []
    regularize = False
    if args.regularize:
        regularize = True
    for cpi in cp:
        e = p.scores(p.closest(cpi))
        e[:len(cpi)] = cpi
        xyz = p.unmap(e, regularize=regularize)
        newxyz.append(xyz)
    t2 = copy.copy(t)
    t2.xyz = np.array(newxyz) * 0.1 # in nanometers
    logfile.write("CoCo has generated a new ensemble of {} structures.\n".format(n_structures))

    t = t + t2
    p = pca.fromtrajectory(t, quality=95)
    m = mapping.Map(p.projs[:2].T, resolution=5)
    coverage = int(100 * float(len(np.where(m._H > 0)[0])) / 25)
    logfile.write("Combined coverage of the input and CoCo ensembles: {}%\n\n".format(coverage))

    t2.xyz = t2.xyz
    t2.save(args.pdbout)

    logfile.write("CoCo job completed at {}, run time {:6.2f} seconds.\n".format(time.ctime(), time.time() - start_time))
    logfile.write("Thank you for using CoCo, good luck with your research")
    logfile.close()

################################################################################
#                                                                              #
#                                    ENTRY POINT                               #
#                                                                              #
################################################################################

if __name__ == '__main__':
    parser=argparse.ArgumentParser()
    parser.add_argument('pdbin', type=str, help='The multimodel PDB file to process.')
    parser.add_argument('pdbout', type=str, help='The new multimodel PDB file that will be produced.')
    parser.add_argument('logfile', type=str, default=None, help='The log file.')
    parser.add_argument('-r', '--regularize', action='store_true', help='Regularize structures.')
    
    args=parser.parse_args()
    coco_web(args)
